package com.inkglobal.techtest;

import org.junit.Assert;
import org.junit.Test;

public class BerlinClockTest {
    BerlinClock berlinClock = new BerlinClock();
    
    @Test(expected=NumberFormatException.class)
    public void testInvalidTime(){    	
    	berlinClock.getBerlinTime("12:34");
    }
   
   @Test
    public void testBerlinClockShouldResultInCorrectSecondsHoursAndMinutes() {
        String[] berlinTime = berlinClock.getBerlinTime("16:37:16");
        String[] expected = new String[] {"Y", "RRRO", "ROOO", "YYRYYRYOOOO", "YYOO"};
        Assert.assertEquals(expected.length, berlinTime.length);
        for (int index = 0; index < expected.length; index++) {
            Assert.assertEquals(expected[index], berlinTime[index]);
        }
    }
	
	@Test
    public void testTopMinutesShouldHave3rd6thAnd9thLampsInRedToIndicateFirstQuarterHalfAndLastQuarter() {
        String minutes32 = berlinClock.getThirdRowMinutes(32);
        Assert.assertEquals("R", minutes32.substring(2, 3));
        Assert.assertEquals("R", minutes32.substring(5, 6));
        Assert.assertEquals("O", minutes32.substring(8, 9));
    }
   
    @Test
    public void testBottomHoursShouldLightRedLampForEveryHourLeftFromTopHours() {
        Assert.assertEquals("OOOO", berlinClock.getSecondRowHours(0));
        Assert.assertEquals("RRRO", berlinClock.getSecondRowHours(13));
        Assert.assertEquals("RRRO", berlinClock.getSecondRowHours(23));
        Assert.assertEquals("RRRR", berlinClock.getSecondRowHours(24));
    } 
    
    @Test
    public void testTopMinutesShouldHave11Lamps() {
        Assert.assertEquals(11, berlinClock.getThirdRowMinutes(34).length());
    }
    
    @Test
    public void testTopMinutesShouldLightYellowLampForEvery5MinutesUnlessItIsFirstQuarterHalfOrLastQuarter() {
        Assert.assertEquals("OOOOOOOOOOO", berlinClock.getThirdRowMinutes(0));
        Assert.assertEquals("YYROOOOOOOO", berlinClock.getThirdRowMinutes(17));
        Assert.assertEquals("YYRYYRYYRYY", berlinClock.getThirdRowMinutes(59));
    }
	
	@Test
    public void testTopHoursShouldLightRedLampForEvery5Hours() {
        Assert.assertEquals("OOOO", berlinClock.getFirstRowHours(0));
        Assert.assertEquals("RROO", berlinClock.getFirstRowHours(13));
        Assert.assertEquals("RRRR", berlinClock.getFirstRowHours(23));
        Assert.assertEquals("RRRR", berlinClock.getFirstRowHours(24));
    }
 
   
    @Test
    public void testBottomHoursShouldHave4Lamps() {
        Assert.assertEquals(4, berlinClock.getSecondRowHours(5).length());
    }
 
    
    @Test
    public void testBottomMinutesShouldHave4Lamps() {
        Assert.assertEquals(4, berlinClock.getFourthRowMinutes(0).length());
    }
 
    
    @Test
    public void testBottomMinutesShouldLightYellowLampForEveryMinuteLeftFromTopMinutes() {
        Assert.assertEquals("OOOO", berlinClock.getFourthRowMinutes(0));
        Assert.assertEquals("YYOO", berlinClock.getFourthRowMinutes(17));
        Assert.assertEquals("YYYY", berlinClock.getFourthRowMinutes(59));
    }
 
    
    @Test
    public void testBerlinClockShouldResultInArrayWith5Elements()  {
        Assert.assertEquals(5, berlinClock.getBerlinTime("13:17:01").length);
    }
	
	@Test
    public void testYellowLampShouldBlinkOnOffEveryTwoSeconds() {
        Assert.assertEquals("Y", berlinClock.getSeconds(0));
        Assert.assertEquals("O", berlinClock.getSeconds(1));
        Assert.assertEquals("Y", berlinClock.getSeconds(4));
    }
 
   @Test
    public void testTopHoursShouldHave4Lamps() {
        Assert.assertEquals(4, berlinClock.getFirstRowHours(7).length());
    }
 
    
    
}
