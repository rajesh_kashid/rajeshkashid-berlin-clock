package com.inkglobal.techtest;

import java.util.ArrayList;
import java.util.List;

public class BerlinClock {
	private static final String REGEX_TIME = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$";
	private static final String YELLOW = "Y";
	private static final String RED = "R";
	private static final String OFF = "O";
	
	private boolean validateTime(String normalTime){
		if(normalTime.matches(REGEX_TIME))
			return true;
		
		return false;		
	}
	
    protected String getSeconds(int secondCounter) {
        if (secondCounter % 2 == 0) return YELLOW;
        else return OFF;
    }

    protected String getFirstRowHours(int hourCounter) {
        return getOnOff(4, getOnSigns(hourCounter));
    }

    protected String getSecondRowHours(int hourCounter) {
        return getOnOff(4, hourCounter % 5);
    }

    protected String getThirdRowMinutes(int minutesCounter) {
        return getOnOff(11, getOnSigns(minutesCounter), YELLOW).replaceAll("YYY", "YYR");
    }

    protected String getFourthRowMinutes(int minutesCounter) {
        return getOnOff(4, minutesCounter % 5, YELLOW);
    }


    private String getOnOff(int noOfLamps, int onSigns) {
        return getOnOff(noOfLamps, onSigns, RED);
    }
    private String getOnOff(int noOfLamps, int onSigns, String onSign) {
        String onOff = "";

        for (int i = 0; i < onSigns; i++) {
            onOff += onSign;
        }
        for (int i = 0; i < (noOfLamps - onSigns); i++) {
            onOff += OFF;
        }
        return onOff;
    }

    private int getOnSigns(int number) {
        return (number - (number % 5)) / 5;
    }
    
    public String[] getBerlinTime(String normalTime) {
        
		if(validateTime(normalTime)){
			List<Integer> parts = new ArrayList<Integer>();
	        for (String part : normalTime.split(":")) {
	            parts.add(Integer.parseInt(part));
	        }
	        return new String[] {
	                getSeconds(parts.get(2)),
	                getFirstRowHours(parts.get(0)),
	                getSecondRowHours(parts.get(0)),
	                getThirdRowMinutes(parts.get(1)),
	                getFourthRowMinutes(parts.get(1))
	        };
		}else{
			throw new NumberFormatException("Invalid Time format");
		}
    }
}
